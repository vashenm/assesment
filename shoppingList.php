<?php
session_start();
include_once("config.php");
$currentUrl = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>List</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</head>
    <body>
        <div class="container">
            <?php
            if(isset($_SESSION["list"]) )
            {
                echo '<div class="d-flex">';
                $form = <<<EOT
                    <form method="post" action="listUpdate.php">
                    <label>
                        <span>Item</span>
                        <input type="text" name="item" />
                    </label>
                    <input type="hidden" name="list_id" value="{$_SESSION['list']}" />
                    <input type="hidden" name="type" value="add" />
                    <input type="hidden" name="returnUrl" value="{$currentUrl}" />
                    <button type="submit" class=" btn btn-primary mt-2">Add</button>
                </form>
                EOT;
                echo $form;
                echo'</div>';

                if(isset($_SESSION["items"])){
                    echo '<div class="my-5 border p-4">';
                    echo '<h3>Your Cart</h3>';
                    echo '<form method="post" action="listUpdate.php">';
                    echo '<table class="table" width="100%"  cellpadding="6" cellspacing="0">';
                    echo '<tbody>';

                    foreach ($_SESSION["items"] as $itm)
                    {
      
                        $item = $itm->item;
                        $id = $itm->id;
                        echo '<tr class="">';
                        echo '<td>'.$item.'</td>';
                        echo '<td><input type="checkbox" name="remove[]" value="'.$id.'" /> Remove</td>';
                        if($itm->complete){
                            echo '<td>Completed</td>';
                        } else{
                            echo '<td><input type="checkbox" name="complete[]" value="'.$id.'" /> Complete</td>';
                        }
                        echo '<td><a href="editItem.php?id='.$id.'">Edit</a>';
                        echo '</tr>';
                    }
                    echo '<td colspan="">';
                    echo '<button class="btn btn-secondary" type="submit">Update</button>';
                    echo '</td>';
    
                    echo '<td colspan="1">';
                    echo '</td>';
                    
                    echo '</tbody>';
                    echo '</table>';
                    
                    $currentUrl = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
                    echo '<input type="hidden" name="returnUrl" value="'.$currentUrl.'" />';
                    echo '</form>';
                    echo '</div>';
                }

            } else{

                $mysqli->query("INSERT INTO lists () VALUES ()");
                $_SESSION["list"] = $mysqli->insert_id;
                
                $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                header('Location:'.$url);
            }
            ?>
        </div>   
    </body>
</html>

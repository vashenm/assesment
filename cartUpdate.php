<?php
session_start();
include_once("config.php");

if(isset($_POST["type"]) && $_POST["type"]=='add' && $_POST["qty"]>0)
{
	foreach($_POST as $key => $value){ 
		$new_product[$key] = filter_var($value, FILTER_SANITIZE_STRING);
    }
	unset($new_product['type']);
	unset($new_product['returnUrl']); 
	
    if(isset($_SESSION["cart"])){ 
        if(isset($_SESSION["cart"][$new_product['sku']]))
        {
            $_SESSION["cart"][$new_product['sku']]['qty'] = $_SESSION["cart"][$new_product['sku']]['qty'] + $_POST["qty"];
        }  else{
            $_SESSION["cart"][$new_product['sku']] = $new_product;
        }          
    }else{
        $_SESSION["cart"][$new_product['sku']] = $new_product; //update or create product session with new item  
    }
}

if(isset($_POST["qty"]) || isset($_POST["remove_code"]))
{
	if(isset($_POST["qty"]) && is_array($_POST["qty"])){
		foreach($_POST["qty"] as $key => $value){
			if(is_numeric($value)){
				$_SESSION["cart"][$key]["qty"] = $value;
			}
		}
	}
    
	if(isset($_POST["remove_code"]) && is_array($_POST["remove_code"])){
		foreach($_POST["remove_code"] as $key){
			unset($_SESSION["cart"][$key]);
		}	
	}
}

$returnUrl = (isset($_POST["returnUrl"]))?urldecode($_POST["returnUrl"]):''; //return url
header('Location:'.$returnUrl);
?>
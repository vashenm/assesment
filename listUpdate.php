<?php
session_start();
include_once("config.php");

function getItems($mysqli){
	$results = $mysqli->query("SELECT id, item,complete FROM items WHERE list_id = {$_SESSION["list"]} ");
	$_SESSION["items"] = array();
	if($results){ 
		while($obj = $results->fetch_object())
		{
			
			$_SESSION["items"][] = $obj;			
		}
	}
}

if(isset($_POST["type"]) && $_POST["type"]=='add' && isset($_POST["item"]))
{

	$mysqli->query("INSERT INTO items (item,list_id) VALUES ('{$_POST["item"]}','{$_SESSION["list"]}')");
	getItems($mysqli);

}

if(isset($_POST["type"]) && $_POST["type"]=='update' && isset($_POST["item"]))
{

	$mysqli->query("UPDATE items SET item = '{$_POST["item"]}', complete = '0'  WHERE id = {$_POST["id"]}");
	getItems($mysqli);

}

if(isset($_POST["remove"]))
{
	foreach($_POST["remove"] as $key => $value){
		if(is_numeric($value)){
			$mysqli->query("DELETE from items WHERE id = {$value} ");
		}
	}
	getItems($mysqli);
}

if(isset($_POST["complete"]))
{

	foreach($_POST["complete"] as $key => $value){
		if(is_numeric($value)){
			$mysqli->query("UPDATE items SET complete = '1' WHERE id = {$value} ");
		}
	}
	getItems($mysqli);
}

$returnUrl = (isset($_POST["returnUrl"]))?urldecode($_POST["returnUrl"]):''; //return url
header('Location:'.$returnUrl);
?>
<?php
session_start();
include_once("config.php");


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>List</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</head>
    <body>
        <div class="container">
            <?php
                $results = $mysqli->query("SELECT item,id FROM items where id =".$_GET['id']);
                if($results){ 
                    $products_item = '<div class="my-5 d-flex just justify-content-between products">';
                    //fetch results set as object and output HTML
                    while($obj = $results->fetch_object()){
                    
                        echo '<div class="d-flex">';
                        $form = <<<EOT
                            <form method="post" action="listUpdate.php">
                            <label>
                                <span>Item</span>
                                <input type="text" name="item" value="{$obj->item}" />
                            </label>
                            <input type="hidden" name="type" value="update" />
                            <input type="hidden" name="id" value="{$obj->id}" />
                            <input type="hidden" name="returnUrl" value="shoppingList.php" />
                            <button type="submit" class=" btn btn-primary mt-2">Update</button>
                        </form>
                        EOT;
                        echo $form;
                        echo'</div>';
                    }
                }
            
            ?>
        </div>   
    </body>
</html>

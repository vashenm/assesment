CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(60) NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` tinytext NOT NULL,
  `image` varchar(60) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sku` (`sku`)
) AUTO_INCREMENT=1 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `description`, `image`, `price`) VALUES
(1, 'PD1001', 'Product 1', 'lorem ipsum description 1', 'image1.png', 200.50),
(2, 'PD1002', 'Product 2', 'lorem ipsum description 2', 'image2.png', 500.85),
(3, 'PD1003', 'Product 3', 'lorem ipsum description 3', 'image3.png', 100.00),
(4, 'PD1004', 'Product 4', 'lorem ipsum description 4', 'image4.png', 400.30);


CREATE TABLE IF NOT EXISTS `lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(255) NOT NULL,
  `complete` boolean not null default 0,
  `list_id` int,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`list_id`) REFERENCES lists(`id`)
) AUTO_INCREMENT=1 ;

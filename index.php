<?php
session_start();
include_once("config.php");
$currentUrl = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shopping Cart</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</head>
    <body>
        <div class="container">
            <?php
            if(isset($_SESSION["cart"]) && count($_SESSION["cart"])>0)
            {
                echo '<div class="my-5 border p-4">';
                echo '<h3>Your Cart</h3>';
                echo '<form method="post" action="cartUpdate.php">';
                echo '<table class="table" width="100%"  cellpadding="6" cellspacing="0">';
                echo '<tbody>';

                $total =0;
                $b = 0;
                foreach ($_SESSION["cart"] as $cartItem)
                {
                    $name = $cartItem["name"];
                    $qty = $cartItem["qty"];
                    $price = $cartItem["price"];
                    $sku = $cartItem["sku"];
                    $subtotal = ($price * $qty);
                    $total = ($total + $subtotal);
                    echo '<tr class="">';
                    echo '<td>Qty <input type="text" size="2" maxlength="2" name="qty['.$sku.']" value="'.$qty.'" /></td>';
                    echo '<td>'.$name.'</td>';
                    echo '<td> R'.$subtotal.'</td>';
                    echo '<td><input type="checkbox" name="remove_code[]" value="'.$sku.'" /> Remove</td>';
                    echo '</tr>';

                }
                echo '<td colspan="">';
                echo '<button class="btn btn-secondary" type="submit">Update</button>';
                echo '</td>';

                echo '<td colspan="">';
                echo 'Total R'.$total;
                echo '</td>';

                echo '<td colspan="1">';
                echo '</td>';
                
                echo '</tbody>';
                echo '</table>';
                
                $currentUrl = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
                echo '<input type="hidden" name="returnUrl" value="'.$currentUrl.'" />';
                echo '</form>';
                echo '</div>';

            }
            ?>
            <?php
            $results = $mysqli->query("SELECT sku, name, description, image, price FROM products ORDER BY id ASC");
            if($results){ 
            $products_item = '<div class="my-5 d-flex just justify-content-between products">';
            //fetch results set as object and output HTML
            while($obj = $results->fetch_object())
            {
            $products_item .= <<<EOT
                <div class="card col-3 h-auto">
                    <img src="images/{$obj->image}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">{$obj->name} - <span class="small">R{$obj->price}</span></h5>
                        <p class="card-text">{$obj->description}</p>

                    </div>
                    <div class="mt-auto p-4">
                    <form method="post" action="cartUpdate.php">
                        <label>
                            <span>Quantity</span>
                            <input type="text" size="2" maxlength="2" name="qty" value="1" />
                        </label>
                        <input type="hidden" name="sku" value="{$obj->sku}" />
                        <input type="hidden" name="name" value="{$obj->name}" />
                        <input type="hidden" name="price" value="{$obj->price}" />
                        <input type="hidden" name="type" value="add" />
                        <input type="hidden" name="returnUrl" value="{$currentUrl}" />
                        <button type="submit" class="add_to_cart btn btn-primary mt-2">Add</button>
                    </form>
                </div>
                </div>
            EOT;
            }
            $products_item .= '</div>';
            echo $products_item;
            }
            ?>
        </div>   
    </body>
</html>
